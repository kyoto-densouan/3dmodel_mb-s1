# README #

1/3スケールの日立 MB-S1/10風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- 日立

## 発売時期
- 1984年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%99%E3%83%BC%E3%82%B7%E3%83%83%E3%82%AF%E3%83%9E%E3%82%B9%E3%82%BF%E3%83%BC#S1)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_mb-s1/raw/afc7e75ab7e77fc1fb8a8069ab5218d62dcc3061/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mb-s1/raw/afc7e75ab7e77fc1fb8a8069ab5218d62dcc3061/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mb-s1/raw/afc7e75ab7e77fc1fb8a8069ab5218d62dcc3061/ExampleImage.png)
